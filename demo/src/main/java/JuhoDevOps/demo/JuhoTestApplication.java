package JuhoDevOps.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JuhoTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(JuhoTestApplication.class, args);
	}

}
